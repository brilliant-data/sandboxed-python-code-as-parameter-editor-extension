
// The name of the parameter contining the code
export const DEFAULT_PARAMETER_NAME = "Python Code to Evaluate";

// The key in the extension settings where we save the parameter name to
export const PARAMETER_NAME_KEY = "parameter_name";

// The setting name of the programming language
export const LANGUAGE_KEY = "language";

// The deefault langauge to use
export const DEFAULT_LANGUAGE_NAME = "python";

// The list of available languages. name is how prism refers to them, label is
// how the user refers to them
export const AVAILABLE_LANGAUGES = [
  { name: "haskell", label: "Haskell"},
  { name: "matlab", label: "MATLAB"},
  { name: "py", label: "Python"},
  { name: "r", label: "R"},
];
