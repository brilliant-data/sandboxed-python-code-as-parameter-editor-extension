import PrismEditor from 'vue-prism-editor'
import * as TableauHelper from './tableau.js'
import { DEFAULT_PARAMETER_NAME, PARAMETER_NAME_KEY, DEFAULT_LANGUAGE_NAME, LANGUAGE_KEY } from './shared.js'


// Vue.component('prism-editor', VuePrismEditor);

export default {
  name: 'App',
  components: {
    PrismEditor
  },
  data() {
    return  {
      parameterName: DEFAULT_PARAMETER_NAME,
      code: `return "Hello World"`,
      parameters: [],
      language: DEFAULT_LANGUAGE_NAME,
      // contains the last read code from the Tableau parameter
      // Can be used to check for changes
      codeFromTableau: null,

      parameterFound: false,
      isLoading: false,

      isMetaKeyDown: false,
    }
  },
  computed: {
    // are there changes from the Tableau version of the code?
    haveChanges() {
      return this.code !== this.codeFromTableau;
    }
  },
  methods: {
    // attempts to load the saved parameter name from the Tableau settings
    loadConfigFromTableau() {
      this.parameterName = TableauHelper.loadSetting(PARAMETER_NAME_KEY, DEFAULT_PARAMETER_NAME);
      this.language = TableauHelper.loadSetting(LANGUAGE_KEY, DEFAULT_LANGUAGE_NAME);
      // return a fake promise just of consistency, as this is part of the boot process
      return Promise.resolve();
    },

    // Reads a list of all parameters from Tableau, and keeps only the string params
    readAllParametersFromTableau() {
      const STRING_TYPE = TableauHelper.DataType.String;
      return TableauHelper.getAllParameters()
        .then((params) => {
          // only care about string params
          let stringParams = params.filter(param => param.dataType === STRING_TYPE);
          // get the name of each parameter and use that as our data
          let parameterNames = stringParams.map(param => param.name);

          console.log('parameterNames = ', parameterNames);
          this.parameters = parameterNames;
        })
    },

    // Attempts to get the value of a parameter
    readFromTableau() {
      let {parameterName} = this;
      console.log("readFromTableau: ", parameterName);
      return this.withLoading(() => TableauHelper.getParameterValue(parameterName))
        .then(value => {
          console.log("readFromTableau success: ", parameterName, "value=", value);
          // set to empty string and signal
          if (typeof value === 'undefined') {
            this.codeFromTableau = "";
            this.parameterFound = false;
            return;
          }

          this.codeFromTableau = value;
          this.code = TableauHelper.decodeParameterValue(value);
          this.parameterFound = true;
          this.setLogoOpacity(this.code);
        })
        .catch(err => console.error("While reading parameter value from Tableau:", err));
    },

    // attempts to save the code to the Tableau Parameter
    saveToTableau() {
      let transformedCode = TableauHelper.encodeParameterValue(this.code);
      return this.withLoading(() => TableauHelper.setParametersAsync({[this.parameterName]: transformedCode})
        // re-read the value after saving
        .then(() => this.readFromTableau()));
    },

    // Wraps a function returning a promise with loading start/stop
    withLoading(promiseFn) {
      this.startLoading();
      return promiseFn()
        .then(v => { this.stopLoading(); return v; })
        .catch(err => { this.stopLoading(); return Promise.reject(err); });
    },

    startLoading() {
      this.isLoading = true;
    },

    stopLoading() {
      this.isLoading = false;
    },


    showConfiguration() {
      return window.tableau.extensions.ui.displayDialogAsync("configure.html", '', { height: 350, width: 500 })
        .catch((error) => {
          // Popup is closed by the user
          switch(error.errorCode) {
            case window.tableau.ErrorCodes.DialogClosedByUser:
              break;
            default:
              console.error(error.message);
          }
        })
        .then(() => { this.refresh(); });
    },

    // Reload eveerything from Tableau and the settings
    refresh() {
      return this.readAllParametersFromTableau()
        .then(() => this.loadConfigFromTableau())
        .then(() => this.readFromTableau())
    },

    // hide the logo
    onCodeChanged(evt) {
      let newCode = evt.target.textContent;
      this.setLogoOpacity(newCode);
    },

    setLogoOpacity(newCode) {
      let lineCount =  newCode.trim().split(/\r\n|\r|\n/).length;

      let logo = document.querySelector("#logo")
      if (!logo) {
        console.error("Cannot find logo to show/hide");
        return;
      }
      let newOpacity = "0.5";
      if (lineCount > 4) {
        newOpacity = "0.0";
      } else {
        newOpacity = "0.5";
      }

      if (logo.style.opacity != newOpacity) {
        logo.style.opacity = newOpacity;
      }
    },


    onKeyDown(evt) {
      if (isMetaKey(evt)) {
        this.isMetaKeyDown = true;
      }
      if (this.isMetaKeyDown && isEnterKey(evt)) {
        this.saveToTableau();
      }
    },

    onKeyUp(evt) {
      if (isMetaKey(evt)) {
        this.isMetaKeyDown = false;
      }
    }
  },



  mounted() {
    TableauHelper.boot({'configure': () => this.showConfiguration()})
      .then(() => this.refresh())

      .then(() => {console.log("init done", this.$el);})
      .catch(err => console.error("Error while initializing:", err));
  },
}




function isMetaKey(evt) {
  return (evt.key === "Meta" || evt.key === "Control");
}

function isEnterKey(evt) {
  return evt.key === "Enter";
}
