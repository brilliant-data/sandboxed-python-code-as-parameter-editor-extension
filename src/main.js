import Vue from 'vue'
import App from './App.vue'
import Configure from './Configure.vue'

Vue.config.productionTip = false


let isConfigMode = /configure/.test(document.location.href);

let targetModule = isConfigMode ? Configure : App;

new Vue({
  render: h => h(targetModule),
}).$mount('#app')
