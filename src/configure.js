import * as TableauHelper from './tableau.js'
import { DEFAULT_PARAMETER_NAME, PARAMETER_NAME_KEY, DEFAULT_LANGUAGE_NAME, AVAILABLE_LANGAUGES, LANGUAGE_KEY } from './shared.js'

export default {
  name: "Configure",

  data() {
    return {
      parameters: [],
      parameterName: DEFAULT_PARAMETER_NAME,
      language: DEFAULT_LANGUAGE_NAME,

      languages: AVAILABLE_LANGAUGES,
    }
  },

  methods: {
    // attempts to load the saved parameter name from the Tableau settings
    loadSettingsFromTableau() {
      this.parameterName = TableauHelper.loadSetting(PARAMETER_NAME_KEY, DEFAULT_PARAMETER_NAME);
      this.language = TableauHelper.loadSetting(LANGUAGE_KEY, DEFAULT_LANGUAGE_NAME);
      // return a fake promise just of consistency, as this is part of the boot process
      return Promise.resolve();
    },


    // Reads a list of all parameters from Tableau, and keeps only the string params
    readAllParametersFromTableau() {
      const STRING_TYPE = TableauHelper.DataType.String;
      return TableauHelper.getAllParameters()
        .then((params) => {
          // only care about string params
          let stringParams = params.filter(param => param.dataType === STRING_TYPE);
          // get the name of each parameter and use that as our data
          let parameterNames = stringParams.map(param => param.name);

          console.log('parameterNames = ', parameterNames);
          this.parameters = parameterNames;
        })
    },

    // Setters
    // -------

    setParameterName(newParameterName) {
      // skip doing anything if there is no real change
      if (newParameterName === this.parameterName) {
        return Promise.resolve();
      }
      return TableauHelper.saveSetting(PARAMETER_NAME_KEY, newParameterName)
        .then(() => {
          this.parameterName = newParameterName;
        })
        .then(() => this.readFromTableau());
    },

    setLanguage(newLanguage) {
      // skip doing anything if there is no real change
      if (newLanguage === this.language) {
        return Promise.resolve();
      }
      return TableauHelper.saveSetting(LANGUAGE_KEY, newLanguage)
        .then(() => {
          this.language = newLanguage;
        })
        .then(() => this.readFromTableau());
    },


    // Close the configuration window
    closeWindow() {
      TableauHelper.closeWindow();
    },

  },

  mounted() {
    TableauHelper.boot({configure: () => {}})

      .then(() => this.readAllParametersFromTableau())
      .then(() => this.loadSettingsFromTableau())

      .then(() => {console.log("init done", this.$el);})
      .catch(err => console.error("Error while initializing:", err));
  },
}
